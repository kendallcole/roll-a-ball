﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMove : MonoBehaviour
{
    float directionTime = 1f;
    float currentTimer = 1f;

    float speed = 50f;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
            currentTimer = Time.deltaTime;

            if (currentTimer <= 0)
            {
                transform.RotateAround(Vector3.up,Random.Range(0, 500f));

                currentTimer = directionTime;

                rb.AddForce(transform.forward * speed);
            }
    }
}
