﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour {

    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public float jump;
    public List<GameObject> pickups;
    private Rigidbody rb;
    private int count;
    public GameObject Ghost;

    private float movementX;
    private float movementY;

// Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);

        List<GameObject> pickups = new List <GameObject>();
        pickups.Add(new GameObject ("PickUp"));
        pickups.Add(new GameObject ("PickUp(1)"));
        pickups.Add(new GameObject ("PickUp(2)"));
        pickups.Add(new GameObject ("PickUp(3)"));
        pickups.Add(new GameObject ("PickUp(4)"));
        pickups.Add(new GameObject ("PickUp(5)"));
        pickups.Add(new GameObject ("PickUp(6)"));
        pickups.Add(new GameObject ("PickUp(7)"));
        pickups.Add(new GameObject ("PickUp(8)"));
        pickups.Add(new GameObject ("PickUp(9)"));
        pickups.Add(new GameObject ("PickUp(10)"));
        pickups.Add(new GameObject ("PickUp(11)"));
        pickups.Add(new GameObject ("PickUp(12)"));

    }
    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12)
        {
            winTextObject.SetActive(true);
        }
    }
    private void FixedUpdate ()
    {

        Vector3 movement = new Vector3(movementX, jump, movementY);

        rb.AddForce(movement * speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }

        if (other.gameObject.CompareTag("Ghost"))
        {
            bool hasFoundOne = false;
            for (int i = 0; i < pickups.Count; i++)
            {
                if (!pickups[i].activeSelf && !
            hasFoundOne)
            {
                hasFoundOne = true;
                pickups[i].SetActive(true);
            }

        }

    }
 }}
